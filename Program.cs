﻿using System;

namespace exercise_20
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();
            //Task A - compare 2 numbers entered by user
            //Declare variables
            var userAge = 0;
            var friendAge = 0;

            //Ask the user to enter a number. Store as var userAge
            Console.WriteLine("Please enter your age.");
            userAge = int.Parse(Console.ReadLine());

            //Ask the user to enter the age of their friend. Store as var friendAge
            Console.WriteLine("Please enter the age of your best friend.");
            friendAge = int.Parse(Console.ReadLine());

            //if statement checking if both var are equal - if condition is true run code block
            if  (userAge==friendAge)
            {
                Console.WriteLine("You are the same age as your best friend.");
            }
            //else if both var are not equal then print else code block to screen
            else
            {
                Console.WriteLine("You are not the same age as your best friend.");
            }
            Console.ReadKey();

            //Clear the console
            Console.Clear();

            //Task B - use .Length to check password char count
            //Declare variables
            var password = "";

            //Ask user to enter a password. Store as var password
            Console.WriteLine("Please enter a new password at least 8 characters in length...");
            password = Console.ReadLine();

            if (password.Length < 8)
            {
                Console.WriteLine("Sorry the password you have entered is too short");
            }
            Console.WriteLine("Great! The password you have entered has successfully been set!");
            Console.ReadKey();
            
            
            //Task C - change user password using .Replace
            //Clear Console
            Console.Clear();

            //Declare variables
            var passOrig = "admin";
            var passNew = "";
            
            //Ask user to enter the default password 'admin' - then proceed to change
            Console.WriteLine("Please enter the default password to continue.");
            Console.WriteLine($"(Default password = '{passOrig}')");
            var initialInput = Console.ReadLine();

            //if condition true execute code block
            if (initialInput==passOrig)
            {
                //ask user to enter new password -store as var passNew
                Console.WriteLine("Great! Please enter a new password to change from the default.");
                Console.WriteLine("Note: New Password must be at least 8 characters long to be valid.");
                passNew = Console.ReadLine();

                //nested if statement-check if password less than 8 characters
                if (passNew.Length < 8)
                {
                    Console.WriteLine("Sorry you have entered an invalid password :(");
                }
                else if (passNew==passOrig)//check new/old passwords do not match
                {
                    Console.WriteLine("Sorry you have entered an invalid password :(");
                }
                else 
                {
                    Console.WriteLine("Great! Your password has succefully been updated :)");
                    passOrig = passOrig.Replace(passOrig,passNew);
                    Console.WriteLine($"Your new password is {passOrig}");
                }

            }
            else 
            {
                Console.WriteLine("Sorry the password you have entered does not match the default :(");
            }   
            
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to end the program");
            Console.ReadKey();
            
            



            




        }
    }
}
